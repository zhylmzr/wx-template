### 项目结构
- 小程序开发者工具配置文件 `project.config.json`
- 小程序根目录 `app`
- typescript 定义文件 `typings` 和 配置文件 `tsconfig.json` 用于开发时的智能提示

### npm 使用
- 如果要在小程序中使用npm包如`lodash`, cd到`app`目录下进行安装, 然后必须`开发者工具`-`Tools`-`Build npm`构建一下
- 项目根目录下的npm包用作辅助开发,一般使用cli类包

### sass 支持
- `npm run css` 或者 `yarn css` 运行sass编译器, 它会监听 `app/pages` 和 `app/styles` 下的`.scss`, `.sass`文件并编译成同名的`.wxss`
