const fs = require('fs')
const path = require('path')
const sass = require('sass')

const roots = ['app/pages', 'app/style']
const extensions = ['.scss', '.sass']

function isDirectory(filePath) {
    return new Promise((resolve, reject) => {
        fs.stat(filePath, (err, stats) => {
            let result = stats.isDirectory()
            if (result) resolve(filePath)
            else reject(filePath)
        })
    })
}

// 第一次编译
function init() {
    for(let root of roots) {
        compileDirectory(root)
    }
}

function compileDirectory(dir) {
    fs.readdir(dir, (err, items) => {
        for (let item of items) {
            let relativePath = path.join(dir, item)
            isDirectory(relativePath).then(filePath => {
                compileDirectory(filePath)
            }).catch(filePath => {
                if (!extensions.includes(path.extname(filePath))) return
                compileFile(filePath)
            })
        }
    })
}

function compileFile(filePath) {
    sass.render({
        file: filePath
    }, (err, result) => {
        console.clear()
        console.log(`watching the style file ...`)
        if (err) {
            console.log(err.formatted)
            return
        }
        let rs = path.parse(filePath)
        let targetPath = path.join(rs.dir, rs.name)
        targetPath += ".wxss"
        buffer2file(result.css, targetPath)
        console.log('compile done.')
    })
}

function buffer2file(buffer, filePath) {
    fs.open(filePath, 'w', (err, fd) => {
        if (err) console.log(filePath, err)
        fs.write(fd, buffer, () => {})
    })
}


function watchMulDirectory() {
    console.log(`watching the style file ...`)
    for (let root of roots) {
        fs.watch(root, { recursive: true }, (type, filename) => {
            let filePath = path.join(root, filename)
            // 删除文件
            if (type === 'rename' && !fs.existsSync(filePath)) return
            // 文件格式过滤
            if (!extensions.includes(path.extname(filePath))) return
            compileFile(filePath)
        })
    }
}

// 开始
init()
watchMulDirectory()